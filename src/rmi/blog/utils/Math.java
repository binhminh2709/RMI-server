package rmi.blog.utils;

import java.rmi.AlreadyBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import rmi.common.Compute;
import rmi.entity.Strategy;

public class Math extends UnicastRemoteObject implements Compute {
	
	private static final long	serialVersionUID	= 1L;
	
	//unicastRemoteObject lam cho thang Math nay san sang cho thang client su dung, ket hop voi Compute
	//de? dua len rmi registry
	@Override
	public int add(int a, int b) throws RemoteException {
		return a + b;
	}
	
	public Math() throws RemoteException {
		
	}
	
	public static void main(String[] args) {
		
		if(System.getSecurityManager() == null) {
			System.setSecurityManager(new RMISecurityManager());
			System.out.println("Security manager istalled...");
		}
		//-Djava.security.policy=server.policy
		
		try {
			Math m = new Math();
			System.out.println("Server is ready...");
			
			//dua math len RMIRegistry
			Registry registry = LocateRegistry.createRegistry(1989);
			//default port 1099 of rmi
			registry.bind("math", m);
			
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (AlreadyBoundException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public String deploy(Strategy strategy) throws RemoteException {
		
		return strategy.getName();
	}
	
}
